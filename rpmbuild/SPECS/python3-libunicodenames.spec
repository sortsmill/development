Name:		python3-libunicodenames
Version:	1.1.0
Release:	1%{?dist}.beta1
Summary:	Python3 extension for retrieving Unicode annotation data
#Group:		
License:	LGPLv3+
URL:		http://sortsmill.bitbucket.org
Source0:	https://bitbucket.org/sortsmill/python-libunicodenames/downloads/python-libunicodenames-%{version}_beta1.tar.xz
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	coreutils
BuildRequires:	pkgconfig
BuildRequires:	python3-devel
BuildRequires:	swig >= 2.0.4
BuildRequires:	libunicodenames-devel >= 1.1.0-1.beta1
BuildRequires:	libunicodenamesxx-devel >= 1.1.0-1.beta1
Requires:	libunicodenames >= 1.1.0-1.beta1
Requires:	libunicodenamesxx >= 1.1.0-1.beta1
Requires:	python3

# Do not check .so files in the python_sitelib directory.
%global __provides_exclude_from ^%{python_sitearch}/.*\\.so$

%description
Python3-LibUnicodeNames makes it easy for your program to retrieve the
information contained in the "NamesList" file that is published by the
Unicode Consortium.

%define python3 /usr/bin/python3
%define __python  %{python3}

%prep
%setup -q -n python-libunicodenames-%{version}_beta1

%build
%configure PYTHON=%{python3}
make %{?_smp_mflags}

# "make check" does not yet work during RPM packaging.
#%%check
#make %%{?_smp_mflags} check

%install
rm -rf %{buildroot}
make %{?_smp_mflags} install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_libdir}/python*/*-packages/*.la

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{python_sitearch}/*.so
%{python_sitelib}/*.py*
%{python_sitelib}/*/*.py*

%doc AUTHORS README COPYING COPYING.LIB ChangeLog

%changelog
* Tue Jul 23 2013 Barry Schwartz <sortsmill@crudfactory.com> 1.1.0-1.beta1
- upstream release 1.1.0_beta1
